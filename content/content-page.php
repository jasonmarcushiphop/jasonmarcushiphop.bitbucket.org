<?php
/**
 *
 * Learn more: http://codex.wordpress.org/Post_Formats
 *
 * @package Cryout Creations
 * @subpackage Parabola
 */

if ( have_posts()  ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
					<?php } else { ?>
						<h1 class="entry-title"><?php the_title(); ?></h1>
					<?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>





<?php
if(get_the_ID()==19){
$args = array( 'posts_per_page' => 10, 'order'=> 'ASC', 'orderby' => 'title' );
$postslist = get_posts( $args );
foreach ( $postslist as $post ) :
  setup_postdata( $post ); ?> 
	<div>
		<?php the_date(); ?>
		
		<?php echo '<h2><a href="'.get_permalink().'">'.get_the_title().'</a></h2>'; ?>   
		<?php the_content(); ?>
	</div>
<?php
endforeach; 
wp_reset_postdata();
}
?>


						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'parabola' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'parabola' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

				<?php  comments_template( '', true );
				endwhile; ?>
